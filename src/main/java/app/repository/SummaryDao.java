package app.repository;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import rbac.pdp.repository.Dao;
import app.domain.Summary;

@Repository
public class SummaryDao extends Dao<Summary, Long> {

	@SuppressWarnings("unchecked")
	public List<Summary> findAll() {
		Query query = getEntityManager().createNamedQuery("Summary.findAll");
		return query.getResultList();
	}
}
