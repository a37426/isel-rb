package app.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Summary.findAll", query = "SELECT s FROM Summary s ORDER BY s.id DESC")
public class Summary {
	@Id
	@GeneratedValue
	private Long id;
	private String content;

	public Summary(String content) {
		super();
		this.content = content;
	}

	public Summary() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
