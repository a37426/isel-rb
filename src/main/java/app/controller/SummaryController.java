package app.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import rbac.common.dto.UserRoleDto;
import rbac.pdp.component.Pdp;
import app.domain.Summary;
import app.resource.SummaryResource;
import app.service.SummaryService;

@Controller
@RequestMapping("/summary")
public class SummaryController {

	@Autowired
	private SummaryService service;

	@Autowired
	private Pdp pdp;

	private String objectToJson(Object o) {
		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json;
		try {
			json = ow.writeValueAsString(o);
		} catch (Exception e) {
			json = "{}";
		}
		return json;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.delete(id);
		ResponseEntity<Void> response = new ResponseEntity<>(HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getId(@PathVariable Long id) {
		Summary summary = service.read(id);
		ResponseEntity<String> response = new ResponseEntity<>(
				objectToJson(summary),
				objectToJson(summary).equals("{}")
						|| objectToJson(summary).equals("null") ? HttpStatus.NOT_FOUND
						: HttpStatus.OK);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public String get() {
		List<Summary> summaries = service.findAll();
		List<SummaryResource> resources = new ArrayList<>();
		for (Summary summary : summaries) {
			SummaryResource resource = new SummaryResource();
			resource.setIdSummary(summary.getId());
			resource.setContent(summary.getContent());
			Link detail = linkTo(SummaryController.class)
					.slash(summary.getId()).withSelfRel();
			resource.add(detail);
			resources.add(resource);
		}

		return objectToJson(resources);
	}

	@RequestMapping(value = "/{cont}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<String> put(@PathVariable String cont) {

		Summary summary = service.create(new Summary(cont));

		SummaryResource resource = new SummaryResource();
		resource.setIdSummary(summary.getId());
		resource.setContent(summary.getContent());
		Link detail = linkTo(SummaryController.class).slash(summary.getId())
				.withSelfRel();
		resource.add(detail);

		ResponseEntity<String> response = new ResponseEntity<>(
				objectToJson(resource),
				resource.equals("{}") || objectToJson(summary).equals("null") ? HttpStatus.NOT_FOUND
						: HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/userRoles", method = RequestMethod.GET)
	public ResponseEntity<String> getUserRoles() {
		List<UserRoleDto> l = pdp.getAllUserRoles();
		String json = objectToJson(l);
		ResponseEntity<String> response = new ResponseEntity<>(json,
				json.equals("{}") || json.equals("null") ? HttpStatus.NOT_FOUND
						: HttpStatus.OK);
		return response;
	}
}
