package app.resource;

import org.springframework.hateoas.ResourceSupport;

public class SummaryResource extends ResourceSupport {
	private Long idSummary;
	private String content;

	public Long getIdSummary() {
		return idSummary;
	}

	public void setIdSummary(Long idSummary) {
		this.idSummary = idSummary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
