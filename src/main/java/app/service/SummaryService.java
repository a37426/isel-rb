package app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.domain.Summary;
import app.repository.SummaryDao;

@Service
public class SummaryService {

	@Autowired
	private SummaryDao dao;

	@Transactional
	public Summary create(Summary s) {
		return dao.create(s);
	}

	public List<Summary> findAll() {
		return dao.findAll();
	}

	public Summary read(Long id) {
		return dao.read(id);
	}

	@Transactional
	public void delete(Long id) {
		dao.delete(id);
	}

}
