package rbac.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import rbac.common.dto.HierarchyRoleDto;
import rbac.common.dto.PermissionDto;
import rbac.common.dto.PermissionRoleDto;
import rbac.common.dto.ResourceDto;
import rbac.common.dto.ResourcePermissionDto;
import rbac.common.dto.RoleDto;
import rbac.common.dto.UserDto;
import rbac.common.dto.UserRoleDto;
import rbac.pdp.component.Pdp;

@Controller
public class PdpController {
	@Autowired
	private Pdp pdp;

	private String formatName(String name) {
		final String P = "";
		String val = "";
		val = P + name + P;
		return val.replace("/", "_").replace("\\", "_").replace(" ", "_");
	}

	private String render() {
		StringBuffer strb = new StringBuffer();

		String parent;
		String child;
		String node;

		strb.append("digraph G { ");
		strb.append("rank=same; rankdir=LR; ");

		for (UserRoleDto ur : pdp.getAllUserRoles()) {
			parent = formatName(ur.getUserName());
			child = formatName(ur.getRoleName());
			strb.append(parent).append(" -> ").append(child).append("; ");
		}

		for (HierarchyRoleDto hr : pdp.getAllRoleHierarchies()) {
			parent = formatName(hr.getParentName());
			child = formatName(hr.getChildName());
			strb.append(parent).append(" -> ").append(child).append("; ");
		}

		for (ResourcePermissionDto rp : pdp.getAllResourcePermissions()) {
			parent = formatName(rp.getPermissionName());
			child = formatName(rp.getResourceName());
			strb.append(parent).append(" -> ").append(child).append("; ");
		}

		for (PermissionRoleDto rp : pdp.getAllPermissionRoles()) {
			parent = formatName(rp.getRoleName());
			child = formatName(rp.getPermissionName());
			strb.append(parent).append(" -> ").append(child).append("; ");
		}

		strb.append("subgraph users {");

		for (UserDto u : pdp.getAllUsers()) {
			node = formatName(u.getName());
			strb.append(node)
					.append(" [color=black, style=filled, fontcolor=white, shape=\"box\"]; ");
		}

		strb.append("} ");

		for (RoleDto r : pdp.getAllRoles()) {
			node = formatName(r.getName());
			strb.append(node)
					.append(" [color=midnightblue, style=filled, fontcolor=white, shape=\"box\"]; ");
		}

		strb.append(" subgraph permissions {");

		for (PermissionDto p : pdp.getAllPermissions()) {
			node = formatName(p.getName());
			strb.append(node)
					.append(" [color=indigo, style=filled, fontcolor=white, shape=\"box\"]; ");
		}

		strb.append("} subgraph resources {");

		for (ResourceDto r : pdp.getAllResources()) {
			node = formatName(r.getName());
			strb.append(node)
					.append(" [color=darkgreen, style=filled, fontcolor=white, shape=\"box\"]; ");
		}
		strb.append("}");
		strb.append("}");

		return strb.toString();
	}

	private ModelAndView returnPage(Map<String, Object> model) {
		if (model == null)
			model = new HashMap<>();

		List<UserDto> users = pdp.getAllUsers();
		List<RoleDto> roles = pdp.getAllRoles();
		List<PermissionDto> permissions = pdp.getAllPermissions();
		List<ResourceDto> resources = pdp.getAllResources();
		List<UserRoleDto> userRoles = pdp.getAllUserRoles();
		List<PermissionRoleDto> permissionRoles = pdp.getAllPermissionRoles();
		List<ResourcePermissionDto> resourcePermissions = pdp
				.getAllResourcePermissions();
		List<HierarchyRoleDto> hRoles = pdp.getAllRoleHierarchies();

		model.put("users", users);
		model.put("roles", roles);
		model.put("permissions", permissions);
		model.put("resources", resources);
		model.put("userRoles", userRoles);
		model.put("permissionRoles", permissionRoles);
		model.put("resourcePermissions", resourcePermissions);
		model.put("hRoles", hRoles);
		model.put("render", render());
		return new ModelAndView("pdp", "model", model);
	}

	private ModelAndView returnPage() {
		return returnPage(null);
	}

	@RequestMapping("/pdp")
	public ModelAndView pdp() {
		return returnPage();
	}

	@RequestMapping("/user")
	public ModelAndView addUser(
			@RequestParam(value = "userName", required = false) String userName) {
		if (userName != null)
			pdp.createUser(userName);
		return returnPage();
	}

	@RequestMapping("/role")
	public ModelAndView addRole(
			@RequestParam(value = "roleName", required = false) String roleName) {
		if (roleName != null)
			pdp.createRole(roleName);
		return returnPage();
	}

	@RequestMapping("/permission")
	public ModelAndView addPermission(
			@RequestParam(value = "permissionName", required = false) String permissionName) {
		if (permissionName != null)
			pdp.createPermission(permissionName);
		return returnPage();
	}

	@RequestMapping("/resource")
	public ModelAndView addResource(
			@RequestParam(value = "resourceName", required = false) String resourceName) {
		if (resourceName != null)
			pdp.createResource(resourceName);
		return returnPage();
	}

	@RequestMapping("/userRole")
	public ModelAndView addUserRole(
			@RequestParam(value = "userId", required = true) Long userId,
			@RequestParam(value = "roleId", required = true) Long roleId) {
		pdp.createUserRole(userId, roleId);
		return returnPage();
	}

	@RequestMapping("/permissionRole")
	public ModelAndView addPermissionRole(
			@RequestParam(value = "permissionId", required = true) Long permissionId,
			@RequestParam(value = "roleId", required = true) Long roleId) {
		pdp.createPermissionRole(permissionId, roleId);
		return returnPage();
	}

	@RequestMapping("/resourcePermission")
	public ModelAndView addResourcePermission(
			@RequestParam(value = "resourceId", required = true) Long resourceId,
			@RequestParam(value = "permissionId", required = true) Long permissionId) {
		pdp.createResourcePermission(resourceId, permissionId);
		return returnPage();
	}

	@RequestMapping("/hRole")
	public ModelAndView addHRole(
			@RequestParam(value = "parentId", required = true) Long parentId,
			@RequestParam(value = "childId", required = true) Long childId) {
		pdp.createRoleHierarchy(parentId, childId);
		return returnPage();
	}

	@RequestMapping("/remUser")
	public ModelAndView removeUser(
			@RequestParam(value = "userSelect", required = true) Long userSelect) {
		pdp.removeUser(userSelect);
		return returnPage();
	}

	@RequestMapping("/remRole")
	public ModelAndView removeRole(
			@RequestParam(value = "roleSelect", required = true) Long roleSelect) {
		pdp.removeRole(roleSelect);
		return returnPage();
	}

	@RequestMapping("/remPermission")
	public ModelAndView removePermission(
			@RequestParam(value = "permissionSelect", required = true) Long permissionSelect) {
		pdp.removePermission(permissionSelect);
		return returnPage();
	}

	@RequestMapping("/remResource")
	public ModelAndView removeResource(
			@RequestParam(value = "resourceSelect", required = true) Long resourceSelect) {
		pdp.removeResource(resourceSelect);
		return returnPage();
	}

	@RequestMapping("/remUserRole")
	public ModelAndView removeUserRole(
			@RequestParam(value = "userRoleSelect", required = true) String userRoleSelect) {
		String[] sCodes = userRoleSelect.split("_");
		Long userId = new Long(sCodes[0]);
		Long roleId = new Long(sCodes[1]);
		pdp.removeUserRole(userId, roleId);
		return returnPage();
	}

	@RequestMapping("/remPermissionRole")
	public ModelAndView removePermissionRole(
			@RequestParam(value = "permissionRoleSelect", required = true) String permissionRoleSelect) {
		String[] sCodes = permissionRoleSelect.split("_");
		Long permissionId = new Long(sCodes[0]);
		Long roleId = new Long(sCodes[1]);
		pdp.removePermissionRole(permissionId, roleId);
		return returnPage();
	}

	@RequestMapping("/remResourcePermission")
	public ModelAndView removeResourcePermission(
			@RequestParam(value = "resourcePermissionSelect", required = true) String resourcePermissionSelect) {
		String[] sCodes = resourcePermissionSelect.split("_");
		Long resourceId = new Long(sCodes[0]);
		Long permissionId = new Long(sCodes[1]);
		pdp.removeResourcePermission(resourceId, permissionId);
		;
		return returnPage();
	}

	@RequestMapping("/remHRole")
	public ModelAndView removeHRole(
			@RequestParam(value = "hRoleSelect", required = true) String hRoleSelect) {
		String[] sCodes = hRoleSelect.split("_");
		Long parentId = new Long(sCodes[0]);
		Long childId = new Long(sCodes[1]);
		pdp.removeRoleHierarchy(parentId, childId);
		return returnPage();
	}

	@RequestMapping("/queryUserRole")
	public ModelAndView queryUserRole(
			@RequestParam(value = "queryUserRoleSelect", required = true) Long id) {
		Map<String, Object> model = new HashMap<>();
		model.put("queryUserRoleId", id);
		model.put("rolesByUser", pdp.getRolesByUser(id));
		return returnPage(model);
	}

	@RequestMapping("/queryPermissionRole")
	public ModelAndView queryPermissionRole(
			@RequestParam(value = "queryPermissionRoleSelect", required = true) Long id) {
		Map<String, Object> model = new HashMap<>();
		model.put("queryPermissionRoleId", id);
		model.put("rolesByPermission", pdp.getRolesByPermission(id));
		return returnPage(model);
	}

	@RequestMapping("/queryPermissionUser")
	public ModelAndView queryUserPermission(
			@RequestParam(value = "queryPermissionUserSelect", required = true) Long id) {
		Map<String, Object> model = new HashMap<>();
		model.put("queryPermissionUserId", id);
		model.put("usersByPermission", pdp.getUsersByPermission(id));
		return returnPage(model);
	}

	@RequestMapping("/queryPermissionResource")
	public ModelAndView queryPermissionResource(
			@RequestParam(value = "queryPermissionResourceSelect", required = true) Long id) {
		Map<String, Object> model = new HashMap<>();
		model.put("queryPermissionResourceId", id);
		model.put("permissionsByResource", pdp.getPermissionsByResource(id));
		return returnPage(model);
	}

	@RequestMapping("/session")
	public ModelAndView session(
			@RequestParam(value = "sessionSelect", required = true) String session) {
		String[] sCodes = session.split("_");
		Long userId = new Long(sCodes[0]);
		Long roleId = new Long(sCodes[1]);
		Map<String, Object> model = new HashMap<>();
		model.put("sessionId", session);
		model.put("sessionResult", pdp.getSession(userId, roleId));
		return returnPage(model);
	}

	@RequestMapping("/hasPermission")
	public ModelAndView hasPermission(
			@RequestParam(value = "hasPermUserId", required = true) Long userId,
			@RequestParam(value = "hasPermRoleId", required = true) Long roleId,
			@RequestParam(value = "hasPermResourceId", required = true) Long resourceId) {

		Map<String, Object> model = new HashMap<>();
		model.put("hasPerm", pdp.hasPermission(userId, roleId, resourceId));
		return returnPage(model);
	}

	@RequestMapping("/hasPermissionName")
	public ModelAndView hasPermission(
			@RequestParam(value = "hasPermUser", required = true) String user,
			@RequestParam(value = "hasPermRole", required = true) String role,
			@RequestParam(value = "hasPermResource", required = true) String resource) {

		Map<String, Object> model = new HashMap<>();
		model.put("hasPermName", pdp.hasPermission(user, role, resource));
		return returnPage(model);
	}

	@RequestMapping("/permissionName")
	public ModelAndView hasPermission(
			@RequestParam(value = "permUser", required = true) String user,
			@RequestParam(value = "permRole", required = true) String role,
			@RequestParam(value = "permPermission", required = true) String permission,
			@RequestParam(value = "permResource", required = true) String resource) {

		Map<String, Object> model = new HashMap<>();
		model.put("permName",
				pdp.hasPermission(user, role, permission, resource));
		return returnPage(model);
	}
}
