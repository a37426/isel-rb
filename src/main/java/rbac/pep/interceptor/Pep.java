package rbac.pep.interceptor;

import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import rbac.pdp.component.Pdp;

public class Pep implements HandlerInterceptor {

	private static final Logger logger = Logger.getLogger(Pep.class);

	@Autowired
	private Pdp pdp;

	private Map<String, String> getHeadersInfo(HttpServletRequest request) {

		Map<String, String> map = new HashMap<String, String>();
		@SuppressWarnings("rawtypes")
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
		}

		return map;
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		logger.debug("METHOD:" + request.getMethod());
		logger.debug("URL:" + request.getRequestURI());

		Principal p = request.getUserPrincipal();
		String user = null;
		user = p.getName();
		logger.debug(p.getName());

		final String HOD_ROLE = "hod";
		final String TEACHER_ROLE = "teacher";
		final String ADM_ROLE = "adm";
		final String STUDENT_ROLE = "student";
		final String COORDINATOR_ROLE = "coordinator";
		final String FACULTY_ROLE = "faculty";
		final String MEMBER_ROLE = "member";

		Map<String, String> headers = getHeadersInfo(request);

		final String SESSION = "session";

		String session = null;

		if (headers.containsKey(SESSION)) {
			logger.debug(SESSION + " " + headers.get(SESSION));
			session = headers.get(SESSION);
		} else if (request.isUserInRole(HOD_ROLE)) {
			logger.debug("ROLE:" + HOD_ROLE);
			session = HOD_ROLE;
		} else if (request.isUserInRole(TEACHER_ROLE)) {
			logger.debug("ROLE:" + TEACHER_ROLE);
			session = TEACHER_ROLE;
		} else if (request.isUserInRole(ADM_ROLE)) {
			logger.debug("ROLE:" + ADM_ROLE);
			session = ADM_ROLE;
		} else if (request.isUserInRole(STUDENT_ROLE)) {
			logger.debug("ROLE:" + STUDENT_ROLE);
			session = STUDENT_ROLE;
		} else if (request.isUserInRole(COORDINATOR_ROLE)) {
			logger.debug("ROLE:" + COORDINATOR_ROLE);
			session = COORDINATOR_ROLE;
		} else if (request.isUserInRole(FACULTY_ROLE)) {
			logger.debug("ROLE:" + FACULTY_ROLE);
			session = FACULTY_ROLE;
		} else if (request.isUserInRole(MEMBER_ROLE)) {
			logger.debug("ROLE:" + MEMBER_ROLE);
			session = MEMBER_ROLE;
		}

		final String RESOURCE = "/rb/summary";

		final String RESOURCE_PEP = "/rb";
		String resource = null;
		if (request.getRequestURI().startsWith(RESOURCE)) {
			resource = RESOURCE;
		} else if (request.getRequestURI().startsWith(RESOURCE_PEP))
			return true;

		String permission = request.getMethod();

		logger.debug("SESSION USER:" + user);
		logger.debug("SESSION ROLE:" + session);
		logger.debug("PERMISSION:" + permission);
		logger.debug("RESOURCE:" + resource);

		Boolean result = pdp.hasPermission(user, session, permission, resource);
		logger.debug("RESULT:" + result);
		return result;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		logger.debug("postHandle");

	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		logger.debug("afterCompletion");

	}

}
