package rbac.common.dto;

import java.io.Serializable;

public class PermissionRoleDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long permissionId;
	private String permissionName;
	private Long roleId;
	private String roleName;

	public PermissionRoleDto(Long permissionId, String permissionName,
			Long roleId, String roleName) {
		super();
		this.permissionId = permissionId;
		this.permissionName = permissionName;
		this.roleId = roleId;
		this.roleName = roleName;
	}

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
