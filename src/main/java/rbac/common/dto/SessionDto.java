package rbac.common.dto;

import java.io.Serializable;

public class SessionDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long userId;
	private String userName;
	private Long parentRoleId;
	private String parentRoleName;
	private Long childRoleId;
	private String childRoleName;
	private Long permissionId;
	private String permissionName;
	private Long resourceId;
	private String resourceName;

	public SessionDto(Long userId, String userName, Long parentRoleId,
			String parentRoleName, Long childRoleId, String childRoleName,
			Long permissionId, String permissionName, Long resourceId,
			String resourceName) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.parentRoleId = parentRoleId;
		this.parentRoleName = parentRoleName;
		this.childRoleId = childRoleId;
		this.childRoleName = childRoleName;
		this.permissionId = permissionId;
		this.permissionName = permissionName;
		this.resourceId = resourceId;
		this.resourceName = resourceName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getParentRoleId() {
		return parentRoleId;
	}

	public void setParentRoleId(Long parentRoleId) {
		this.parentRoleId = parentRoleId;
	}

	public String getParentRoleName() {
		return parentRoleName;
	}

	public void setParentRoleName(String parentRoleName) {
		this.parentRoleName = parentRoleName;
	}

	public Long getChildRoleId() {
		return childRoleId;
	}

	public void setChildRoleId(Long childRoleId) {
		this.childRoleId = childRoleId;
	}

	public String getChildRoleName() {
		return childRoleName;
	}

	public void setChildRoleName(String childRoleName) {
		this.childRoleName = childRoleName;
	}

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

}
