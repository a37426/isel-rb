package rbac.common.dto;

import java.io.Serializable;

public class HierarchyRoleDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long parentId;
	private String parentName;

	private Long childId;
	private String childName;

	public HierarchyRoleDto(Long parentId, String parentName, Long childId,
			String childName) {
		super();
		this.parentId = parentId;
		this.parentName = parentName;
		this.childId = childId;
		this.childName = childName;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Long getChildId() {
		return childId;
	}

	public void setChildId(Long childId) {
		this.childId = childId;
	}

	public String getChildName() {
		return childName;
	}

	public void setChildName(String childName) {
		this.childName = childName;
	}

}
