package rbac.common.dto;

import java.io.Serializable;

public class ResourcePermissionDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long resourceId;
	private String resourceName;
	private Long permissionId;
	private String permissionName;

	public ResourcePermissionDto(Long resourceId, String resourceName,
			Long permissionId, String permissionName) {
		super();
		this.resourceId = resourceId;
		this.resourceName = resourceName;
		this.permissionId = permissionId;
		this.permissionName = permissionName;
	}

	public Long getResourceId() {
		return resourceId;
	}

	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public Long getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

}
