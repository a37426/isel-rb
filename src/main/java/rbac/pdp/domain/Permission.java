package rbac.pdp.domain;

import java.util.Set;

import javax.jdo.annotations.Unique;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
		@NamedQuery(name = "Permission.findAll", query = "SELECT p FROM Permission p ORDER BY p.id"),
		@NamedQuery(name = "Permission.findByName", query = "SELECT p FROM Permission p where p.name = :permissionName") })
public class Permission implements Comparable<Permission> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Unique
	@Basic(optional = false)
	private String name;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Role> roles;

	@ManyToMany(mappedBy = "permissions", fetch = FetchType.EAGER)
	private Set<Resource> resources;

	public Permission() {
		super();
	}

	public Permission(String name) {
		super();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<Resource> getResources() {
		return resources;
	}

	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}

	@Override
	public int compareTo(Permission o) {
		return id.intValue() - o.getId().intValue();
	}

}
