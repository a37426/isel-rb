package rbac.pdp.domain;

import java.util.Set;

import javax.jdo.annotations.Unique;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
		@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r ORDER BY r.id"),
		@NamedQuery(name = "Role.findByName", query = "SELECT r FROM Role r where r.name = :roleName") })
public class Role implements Comparable<Role> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Unique
	@Basic(optional = false)
	private String name;

	@ManyToMany(mappedBy = "roles")
	private Set<User> users;

	@ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
	private Set<Permission> permissions;

	@ManyToMany
	private Set<Role> parents;

	@ManyToMany(mappedBy = "parents", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<Role> children;

	public Role() {
		super();
	}

	public Role(String name) {
		super();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@Override
	public int compareTo(Role o) {
		return id.intValue() - o.getId().intValue();
	}

	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public Set<Role> getChildren() {
		return children;
	}

	public void setChildren(Set<Role> children) {
		this.children = children;
	}

	public Set<Role> getParents() {
		return parents;
	}

	public void setParents(Set<Role> parents) {
		this.parents = parents;
	}

}
