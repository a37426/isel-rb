package rbac.pdp.component.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rbac.common.dto.HierarchyRoleDto;
import rbac.common.dto.PermissionDto;
import rbac.common.dto.PermissionRoleDto;
import rbac.common.dto.ResourceDto;
import rbac.common.dto.ResourcePermissionDto;
import rbac.common.dto.RoleDto;
import rbac.common.dto.SessionDto;
import rbac.common.dto.UserDto;
import rbac.common.dto.UserRoleDto;
import rbac.pdp.component.Pdp;
import rbac.pdp.domain.Permission;
import rbac.pdp.domain.Resource;
import rbac.pdp.domain.Role;
import rbac.pdp.domain.User;
import rbac.pdp.service.PermissionService;
import rbac.pdp.service.ResourceService;
import rbac.pdp.service.RoleService;
import rbac.pdp.service.UserService;

@Component
public class PdpImpl implements Pdp {

	private static final Logger logger = Logger.getLogger(Pdp.class);

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private ResourceService resourceService;

	@Override
	public UserDto createUser(String name) {
		User user = userService.create(new User(name));
		return new UserDto(user.getId(), user.getName());
	}

	@Override
	public List<UserDto> getAllUsers() {
		List<UserDto> usersDto = new ArrayList<>();
		for (User user : userService.getAllUsers())
			usersDto.add(new UserDto(user.getId(), user.getName()));

		return usersDto;
	}

	@Override
	public RoleDto createRole(String name) {
		Role role = roleService.create(new Role(name));
		return new RoleDto(role.getId(), role.getName());
	}

	@Override
	public List<RoleDto> getAllRoles() {
		List<RoleDto> rolesDto = new ArrayList<>();
		for (Role role : roleService.getAllRoles())
			rolesDto.add(new RoleDto(role.getId(), role.getName()));

		return rolesDto;
	}

	@Override
	public PermissionDto createPermission(String name) {
		Permission permission = permissionService.create(new Permission(name));
		return new PermissionDto(permission.getId(), permission.getName());
	}

	@Override
	public List<PermissionDto> getAllPermissions() {
		List<PermissionDto> permissionsDto = new ArrayList<>();
		for (Permission permission : permissionService.getAllPermissions())
			permissionsDto.add(new PermissionDto(permission.getId(), permission
					.getName()));

		return permissionsDto;
	}

	@Override
	public ResourceDto createResource(String name) {
		Resource resource = resourceService.create(new Resource(name));
		return new ResourceDto(resource.getId(), resource.getName());
	}

	@Override
	public List<ResourceDto> getAllResources() {
		List<ResourceDto> resourcesDto = new ArrayList<>();
		for (Resource resource : resourceService.getAllResources())
			resourcesDto.add(new ResourceDto(resource.getId(), resource
					.getName()));
		return resourcesDto;
	}

	@Override
	public void removeUser(Long id) {
		userService.delete(id);
	}

	@Override
	public void removeRole(Long id) {
		roleService.delete(id);
	}

	@Override
	public void removePermission(Long id) {
		permissionService.delete(id);
	}

	@Override
	public void removeResource(Long id) {
		resourceService.delete(id);
	}

	@Override
	public UserRoleDto createUserRole(Long userId, Long roleId) {
		User u = userService.createUserRole(userId, roleId);
		Role r = new Role();
		for (Role role : u.getRoles()) {
			if (role.getId().equals(roleId)) {
				r = role;
				break;
			}
		}

		return new UserRoleDto(u.getId(), u.getName(), r.getId(), r.getName());
	}

	@Override
	public void removeUserRole(Long userId, Long roleId) {
		userService.removeUserRole(userId, roleId);
	}

	@Override
	public List<UserRoleDto> getAllUserRoles() {
		List<UserRoleDto> l = new ArrayList<>();
		for (User u : userService.getAllUsers()) {
			if (u.getRoles() != null) {
				for (Role r : u.getRoles()) {
					l.add(new UserRoleDto(u.getId(), u.getName(), r.getId(), r
							.getName()));
				}
			}
		}
		return l;
	}

	@Override
	public PermissionRoleDto createPermissionRole(Long permissionId, Long roleId) {
		Permission p = permissionService.createPermissionRole(permissionId,
				roleId);
		Role r = new Role();
		for (Role role : p.getRoles()) {
			if (role.getId().equals(roleId)) {
				r = role;
				break;
			}
		}

		return new PermissionRoleDto(p.getId(), p.getName(), r.getId(),
				r.getName());
	}

	@Override
	public void removePermissionRole(Long permissionId, Long roleId) {
		permissionService.removePermissionRole(permissionId, roleId);

	}

	@Override
	public List<PermissionRoleDto> getAllPermissionRoles() {
		List<PermissionRoleDto> l = new ArrayList<>();
		for (Permission p : permissionService.getAllPermissions()) {
			if (p.getRoles() != null) {
				for (Role r : p.getRoles()) {
					l.add(new PermissionRoleDto(p.getId(), p.getName(), r
							.getId(), r.getName()));
				}
			}
		}
		return l;
	}

	@Override
	public ResourcePermissionDto createResourcePermission(Long resourceId,
			Long permissionId) {
		Resource r = resourceService.createResourcePermission(resourceId,
				permissionId);
		Permission p = new Permission();
		for (Permission permission : r.getPermissions()) {
			if (permission.getId().equals(permissionId)) {
				p = permission;
				break;
			}
		}

		return new ResourcePermissionDto(r.getId(), r.getName(), p.getId(),
				p.getName());
	}

	@Override
	public void removeResourcePermission(Long resourceId, Long permissionId) {
		resourceService.removeResourcePermission(resourceId, permissionId);
	}

	@Override
	public List<ResourcePermissionDto> getAllResourcePermissions() {
		List<ResourcePermissionDto> l = new ArrayList<>();
		for (Resource r : resourceService.getAllResources()) {
			if (r.getPermissions() != null) {
				for (Permission p : r.getPermissions()) {
					l.add(new ResourcePermissionDto(r.getId(), r.getName(), p
							.getId(), p.getName()));
				}
			}
		}
		return l;
	}

	@Override
	public HierarchyRoleDto createRoleHierarchy(Long parentId, Long childId) {
		Role p = roleService.addChild(parentId, childId);
		Role c = new Role();
		if (p.getChildren() != null) {
			for (Role child : p.getChildren()) {
				if (child.getId().equals(childId)) {
					c = child;
					break;
				}
			}
		}

		return new HierarchyRoleDto(p.getId(), p.getName(), c.getId(),
				c.getName());
	}

	@Override
	public void removeRoleHierarchy(Long parentId, Long childId) {
		roleService.removeChild(parentId, childId);
	}

	@Override
	public List<HierarchyRoleDto> getAllRoleHierarchies() {
		List<HierarchyRoleDto> l = new ArrayList<>();
		for (Role p : roleService.getAllRoles()) {
			if (p.getChildren() != null) {
				for (Role c : p.getChildren()) {
					l.add(new HierarchyRoleDto(p.getId(), p.getName(), c
							.getId(), c.getName()));
				}
			}
		}
		return l;
	}

	@Override
	public List<RoleDto> getRolesByUser(Long userId) {
		List<RoleDto> l = new ArrayList<>();
		for (Role role : userService.getRolesByUser(userId))
			l.add(new RoleDto(role.getId(), role.getName()));

		return l;
	}

	@Override
	public List<RoleDto> getRolesByPermission(Long permissionId) {
		List<RoleDto> l = new ArrayList<>();
		for (Role role : permissionService.getRolesByPermission(permissionId)) {
			l.add(new RoleDto(role.getId(), role.getName()));
		}
		return l;
	}

	@Override
	public List<UserDto> getUsersByPermission(Long permissionId) {
		Set<User> s = new TreeSet<>();
		Set<Role> roles = permissionService.getRolesByPermission(permissionId);

		for (User u : userService.getAllUsers())
			for (Role r : userService.getRolesByUser(u.getId()))
				if (roles.contains(r))
					s.add(u);

		List<UserDto> l = new ArrayList<>();
		for (User u : s)
			l.add(new UserDto(u.getId(), u.getName()));
		return l;
	}

	@Override
	public List<PermissionDto> getPermissionsByResource(Long resourceId) {
		Resource resource = resourceService.read(resourceId);
		List<PermissionDto> l = new ArrayList<>();
		if (resource.getPermissions() != null)
			for (Permission p : resource.getPermissions())
				l.add(new PermissionDto(p.getId(), p.getName()));

		return l;
	}

	@Override
	public List<SessionDto> getSession(Long userId, Long roleId) {
		List<SessionDto> l = new ArrayList<>();

		User user = userService.read(userId);
		Role parentRole = roleService.read(roleId);

		Set<Role> roles = userService.getRolesBySession(userId, roleId);

		for (Role role : roles) {
			if (role.getPermissions() != null
					&& role.getPermissions().size() > 0) {
				for (Permission permission : role.getPermissions()) {
					Permission p = permissionService.read(permission.getId());
					if (p.getResources() != null && p.getResources().size() > 0) {
						for (Resource resource : p.getResources()) {
							l.add(new SessionDto(user.getId(), user.getName(),
									parentRole.getId(), parentRole.getName(),
									role.getId(), role.getName(), p.getId(), p
											.getName(), resource.getId(),
									resource.getName()));
						}
					}
				}
			}
		}
		return l;
	}

	@Override
	public Boolean hasPermission(Long userId, Long roleId, Long resourceId) {

		User user = userService.read(userId);
		Role parentRole = roleService.read(roleId);

		Set<Role> roles = userService.getRolesBySession(userId, roleId);

		for (Role role : roles) {
			if (role.getPermissions() != null
					&& role.getPermissions().size() > 0) {
				for (Permission permission : role.getPermissions()) {
					Permission p = permissionService.read(permission.getId());
					if (p.getResources() != null && p.getResources().size() > 0) {
						for (Resource resource : p.getResources()) {
							if (resource.getId().equals(resourceId)) {
								SessionDto s = new SessionDto(user.getId(),
										user.getName(), parentRole.getId(),
										parentRole.getName(), role.getId(),
										role.getName(), p.getId(), p.getName(),
										resource.getId(), resource.getName());
								ObjectWriter ow = new ObjectMapper().writer()
										.withDefaultPrettyPrinter();
								String json;
								try {
									json = ow.writeValueAsString(s);
								} catch (Exception e) {
									json = "";
								}
								logger.debug(json);
								return true;
							}
						}
					}
				}
			}
		}
		logger.debug(userId + " with " + roleId + " cannot reach " + resourceId);
		return false;
	}

	@Override
	public Boolean hasPermission(String userName, String roleName,
			String resourceName) {
		User u = userService.getUserByName(userName);
		Role r = roleService.getRoleByName(roleName);
		Resource rs = resourceService.getResourceByName(resourceName);
		if (r != null && r != null && rs != null)
			return hasPermission(u.getId(), r.getId(), rs.getId());
		else
			return false;
	}

	private Boolean hasPermission(Long userId, Long roleId, Long permissionId,
			Long resourceId) {

		User user = userService.read(userId);
		Role parentRole = roleService.read(roleId);

		Set<Role> roles = userService.getRolesBySession(userId, roleId);

		for (Role role : roles) {
			if (role.getPermissions() != null
					&& role.getPermissions().size() > 0) {
				for (Permission permission : role.getPermissions()) {
					if (permission.getId().equals(permissionId)) {

						Permission p = permissionService.read(permission
								.getId());
						if (p.getResources() != null
								&& p.getResources().size() > 0) {
							for (Resource resource : p.getResources()) {
								if (resource.getId().equals(resourceId)) {
									SessionDto s = new SessionDto(user.getId(),
											user.getName(), parentRole.getId(),
											parentRole.getName(), role.getId(),
											role.getName(), p.getId(),
											p.getName(), resource.getId(),
											resource.getName());
									ObjectWriter ow = new ObjectMapper()
											.writer()
											.withDefaultPrettyPrinter();
									String json;
									try {
										json = ow.writeValueAsString(s);
									} catch (Exception e) {
										json = "";
									}
									logger.debug(json);
									return true;
								}
							}
						}
					}
				}
			}
		}
		logger.debug(userId + " with " + roleId + " cannot reach " + resourceId);
		return false;
	}

	@Override
	public Boolean hasPermission(String userName, String roleName,
			String permissionName, String resourceName) {
		User u = userService.getUserByName(userName);
		Role r = roleService.getRoleByName(roleName);
		Permission p = permissionService.getPermissionByName(permissionName);
		Resource rs = resourceService.getResourceByName(resourceName);
		if (r != null && r != null && rs != null && p != null)
			return hasPermission(u.getId(), r.getId(), p.getId(), rs.getId());
		else
			return false;
	}
}
