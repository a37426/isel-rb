package rbac.pdp.component;

import java.util.List;

import rbac.common.dto.HierarchyRoleDto;
import rbac.common.dto.PermissionDto;
import rbac.common.dto.PermissionRoleDto;
import rbac.common.dto.ResourceDto;
import rbac.common.dto.ResourcePermissionDto;
import rbac.common.dto.RoleDto;
import rbac.common.dto.SessionDto;
import rbac.common.dto.UserDto;
import rbac.common.dto.UserRoleDto;

public interface Pdp {
	public UserDto createUser(String name);

	public void removeUser(Long id);

	public List<UserDto> getAllUsers();

	public RoleDto createRole(String name);

	public void removeRole(Long id);

	public List<RoleDto> getAllRoles();

	public PermissionDto createPermission(String name);

	public void removePermission(Long id);

	public List<PermissionDto> getAllPermissions();

	public ResourceDto createResource(String name);

	public void removeResource(Long id);

	public List<ResourceDto> getAllResources();

	public UserRoleDto createUserRole(Long userId, Long roleId);

	public void removeUserRole(Long userId, Long roleId);

	public List<UserRoleDto> getAllUserRoles();

	public PermissionRoleDto createPermissionRole(Long permissionId, Long roleId);

	public void removePermissionRole(Long permissionId, Long roleId);

	public List<PermissionRoleDto> getAllPermissionRoles();

	public ResourcePermissionDto createResourcePermission(Long resourceId,
			Long permissionId);

	public void removeResourcePermission(Long resourceId, Long permissionId);

	public List<ResourcePermissionDto> getAllResourcePermissions();

	public HierarchyRoleDto createRoleHierarchy(Long parentId, Long childId);

	public void removeRoleHierarchy(Long parentId, Long childId);

	public List<HierarchyRoleDto> getAllRoleHierarchies();

	public List<RoleDto> getRolesByUser(Long userId);

	public List<RoleDto> getRolesByPermission(Long permissionId);

	public List<UserDto> getUsersByPermission(Long permissionId);

	public List<PermissionDto> getPermissionsByResource(Long resourceId);

	public List<SessionDto> getSession(Long userId, Long roleId);

	public Boolean hasPermission(Long userId, Long roleId, Long resourceId);

	public Boolean hasPermission(String userName, String roleName,
			String resourceName);

	public Boolean hasPermission(String userName, String roleName,
			String permissionName, String resourceName);

}
