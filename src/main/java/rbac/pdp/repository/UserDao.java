package rbac.pdp.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import rbac.pdp.domain.Role;
import rbac.pdp.domain.User;

@Repository
public class UserDao extends Dao<User, Long> {

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		Query query = getEntityManager().createNamedQuery("User.findAll");
		return query.getResultList();
	}

	public User getUserByName(String userName) {
		Query query = getEntityManager().createNamedQuery("User.findByName")
				.setParameter("userName", userName);
		@SuppressWarnings("unchecked")
		List<User> users = query.getResultList();
		return (users != null && users.size() > 0) ? users.get(0) : null;
	}

	public User createUserRole(Long userId, Long roleId) {
		Role r = getEntityManager().find(Role.class, roleId);
		User u = read(userId);
		if (u.getRoles() == null)
			u.setRoles(new HashSet<Role>());
		u.getRoles().add(r);
		update(u);
		return u;
	}

	public void removeUserRole(Long userId, Long roleId) {
		User u = read(userId);
		Role r = getEntityManager().find(Role.class, roleId);
		if (u.getRoles() != null) {
			u.getRoles().remove(r);
			update(u);
		}
	}

	private Set<Role> getChildrenRole(Role parentRole) {
		Set<Role> l = new TreeSet<>();
		Role p = getEntityManager().find(Role.class, parentRole.getId());
		l.add(p);
		if (p.getChildren() != null && p.getChildren().size() > 0) {
			for (Role role : p.getChildren()) {
				l.add(role);
				l.addAll(getChildrenRole(role));
			}
		}
		return l;
	}

	public Set<Role> getRolesByUser(Long userId) {
		Set<Role> l = new TreeSet<>();
		User user = read(userId);

		if (user.getRoles() != null && user.getRoles().size() > 0) {
			for (Role role : user.getRoles()) {
				l.addAll(getChildrenRole(role));
			}
		}
		return l;
	}

	private Set<Role> findRootRole(Role parentRole, Long roleId) {
		Set<Role> l = new TreeSet<>();
		Role p = getEntityManager().find(Role.class, parentRole.getId());

		if (p.getId().equals(roleId)) {
			l.add(p);
			if (p.getChildren() != null && p.getChildren().size() > 0) {
				for (Role role : p.getChildren()) {
					l.add(role);
					l.addAll(getChildrenRole(role));
				}
			}
		} else if (p.getChildren() != null && p.getChildren().size() > 0) {
			for (Role role : p.getChildren()) {
				l.addAll(findRootRole(role, roleId));
			}
		}
		return l;
	}

	public Set<Role> getRolesBySession(Long userId, Long roleId) {
		Set<Role> l = new TreeSet<>();
		User user = read(userId);

		if (user.getRoles() != null && user.getRoles().size() > 0)
			for (Role role : user.getRoles())
				l.addAll(findRootRole(role, roleId));
		return l;
	}

}
