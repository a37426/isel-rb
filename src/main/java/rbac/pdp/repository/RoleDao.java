package rbac.pdp.repository;

import java.util.HashSet;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import rbac.pdp.domain.Role;

@Repository
public class RoleDao extends Dao<Role, Long> {
	@SuppressWarnings("unchecked")
	public List<Role> getAllRoles() {
		Query query = getEntityManager().createNamedQuery("Role.findAll");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public Role getRoleByName(String roleName) {
		Query query = getEntityManager().createNamedQuery("Role.findByName")
				.setParameter("roleName", roleName);
		List<Role> roles = query.getResultList();
		return (roles != null && roles.size() > 0) ? roles.get(0) : null;
	}

	public Role addChild(Long parentId, Long childId) {
		Role p = read(parentId);
		if (parentId != childId) {
			if (p.getChildren() == null)
				p.setChildren(new HashSet<Role>());
			Role c = read(childId);
			if (c.getParents() == null)
				c.setParents(new HashSet<Role>());
			c.getParents().add(p);
			p.getChildren().add(c);
			update(p);
		}
		return p;
	}

	public void removeChild(Long parentId, Long childId) {
		Role p = read(parentId);
		Role c = read(childId);
		if (p.getChildren() != null) {
			p.getChildren().remove(c);
			update(p);
		}
	}
}
