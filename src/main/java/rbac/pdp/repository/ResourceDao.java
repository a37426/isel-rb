package rbac.pdp.repository;

import java.util.HashSet;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import rbac.pdp.domain.Permission;
import rbac.pdp.domain.Resource;

@Repository
public class ResourceDao extends Dao<Resource, Long> {
	@SuppressWarnings("unchecked")
	public List<Resource> getAllResources() {
		Query query = getEntityManager().createNamedQuery("Resource.findAll");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public Resource getResourceByName(String resourceName) {
		Query query = getEntityManager()
				.createNamedQuery("Resource.findByName").setParameter(
						"resourceName", resourceName);
		List<Resource> resources = query.getResultList();
		return (resources != null && resources.size() > 0) ? resources.get(0)
				: null;
	}

	public Resource createResourcePermission(Long resourceId, Long permissionId) {
		Permission p = getEntityManager().find(Permission.class, permissionId);
		Resource r = read(resourceId);
		if (r.getPermissions() == null)
			r.setPermissions(new HashSet<Permission>());
		r.getPermissions().add(p);
		update(r);
		return r;
	}

	public void removeResourcePermission(Long resourceId, Long permissionId) {
		Permission p = getEntityManager().find(Permission.class, permissionId);
		Resource r = read(resourceId);
		if (r.getPermissions() != null) {
			r.getPermissions().remove(p);
			update(r);
		}
	}
}
