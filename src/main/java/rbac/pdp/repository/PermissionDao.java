package rbac.pdp.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import rbac.pdp.domain.Permission;
import rbac.pdp.domain.Role;

@Repository
public class PermissionDao extends Dao<Permission, Long> {
	@SuppressWarnings("unchecked")
	public List<Permission> getAllPermissions() {
		Query query = getEntityManager().createNamedQuery("Permission.findAll");
		return query.getResultList();
	}

	public Permission createPermissionRole(Long permissionId, Long roleId) {
		Role r = getEntityManager().find(Role.class, roleId);
		Permission p = read(permissionId);
		if (p.getRoles() == null)
			p.setRoles(new HashSet<Role>());
		p.getRoles().add(r);
		update(p);
		return p;
	}

	public void removePermissionRole(Long permissionId, Long roleId) {
		Role r = getEntityManager().find(Role.class, roleId);
		Permission p = read(permissionId);
		if (p.getRoles() != null) {
			p.getRoles().remove(r);
			update(p);
		}
	}

	private Set<Role> getParentRole(Role childRole) {
		Set<Role> l = new TreeSet<>();
		Role p = getEntityManager().find(Role.class, childRole.getId());
		l.add(p);
		if (p.getParents() != null && p.getParents().size() > 0) {
			for (Role role : p.getParents()) {
				l.add(role);
				l.addAll(getParentRole(role));
			}
		}
		return l;
	}

	public Set<Role> getRolesByPermission(Long permissionId) {
		Set<Role> l = new TreeSet<>();
		Permission p = read(permissionId);
		if (p.getRoles() != null && p.getRoles().size() > 0) {
			for (Role r : p.getRoles()) {
				l.addAll(getParentRole(r));
			}
		}
		return l;
	}

	@SuppressWarnings("unchecked")
	public Permission getPermissionByName(String permissionName) {
		Query query = getEntityManager().createNamedQuery(
				"Permission.findByName").setParameter("permissionName",
				permissionName);
		List<Permission> permissions = query.getResultList();
		return (permissions != null && permissions.size() > 0) ? permissions
				.get(0) : null;
	}

}
