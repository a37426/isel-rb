package rbac.pdp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rbac.pdp.domain.Role;
import rbac.pdp.repository.RoleDao;

@Service
public class RoleService {
	@Autowired
	private RoleDao roleDao;

	@Transactional
	public Role create(Role role) {
		return roleDao.create(role);
	}

	@Transactional
	public void delete(Long id) {
		roleDao.delete(id);
	}

	@Transactional
	public Role addChild(Long parentId, Long childId) {
		return roleDao.addChild(parentId, childId);
	}

	@Transactional
	public void removeChild(Long parentId, Long childId) {
		roleDao.removeChild(parentId, childId);
	}

	public List<Role> getAllRoles() {
		return roleDao.getAllRoles();
	}

	public Role read(Long id) {
		return roleDao.read(id);
	}

	public Role getRoleByName(String roleName) {
		return roleDao.getRoleByName(roleName);
	}

}
