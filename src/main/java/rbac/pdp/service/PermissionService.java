package rbac.pdp.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rbac.pdp.domain.Permission;
import rbac.pdp.domain.Role;
import rbac.pdp.repository.PermissionDao;

@Service
public class PermissionService {
	@Autowired
	private PermissionDao permissionDao;

	@Transactional
	public Permission create(Permission permission) {
		return permissionDao.create(permission);
	}

	@Transactional
	public void delete(Long id) {
		permissionDao.delete(id);
	}

	public List<Permission> getAllPermissions() {
		return permissionDao.getAllPermissions();
	}

	@Transactional
	public Permission createPermissionRole(Long permissionId, Long roleId) {
		return permissionDao.createPermissionRole(permissionId, roleId);
	}

	@Transactional
	public void removePermissionRole(Long permissionId, Long roleId) {
		permissionDao.removePermissionRole(permissionId, roleId);
	}

	public Set<Role> getRolesByPermission(Long permissionId) {
		return permissionDao.getRolesByPermission(permissionId);
	}

	public Permission read(Long id) {
		return permissionDao.read(id);
	}

	public Permission getPermissionByName(String permissionName) {
		return permissionDao.getPermissionByName(permissionName);
	}

}
