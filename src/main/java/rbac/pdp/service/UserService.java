package rbac.pdp.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rbac.pdp.domain.Role;
import rbac.pdp.domain.User;
import rbac.pdp.repository.UserDao;

@Service
public class UserService {
	@Autowired
	private UserDao userDao;

	@Transactional
	public User create(User user) {
		return userDao.create(user);
	}

	@Transactional
	public void delete(Long id) {
		userDao.delete(id);
	}

	@Transactional
	public User createUserRole(Long userId, Long roleId) {
		return userDao.createUserRole(userId, roleId);
	}

	@Transactional
	public void removeUserRole(Long userId, Long roleId) {
		userDao.removeUserRole(userId, roleId);
	}

	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

	public Set<Role> getRolesByUser(Long userId) {
		return userDao.getRolesByUser(userId);
	}

	public Set<Role> getRolesBySession(Long userId, Long roleId) {
		return userDao.getRolesBySession(userId, roleId);
	}

	public User read(Long id) {
		return userDao.read(id);
	}

	public User getUserByName(String userName) {
		return userDao.getUserByName(userName);
	}

}
