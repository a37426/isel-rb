package rbac.pdp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rbac.pdp.domain.Resource;
import rbac.pdp.repository.ResourceDao;

@Service
public class ResourceService {
	@Autowired
	private ResourceDao resourceDao;

	@Transactional
	public Resource create(Resource resource) {
		return resourceDao.create(resource);
	}

	@Transactional
	public void delete(Long id) {
		resourceDao.delete(id);
	}

	@Transactional
	public Resource createResourcePermission(Long resourceId, Long permissionId) {
		return resourceDao.createResourcePermission(resourceId, permissionId);
	}

	@Transactional
	public void removeResourcePermission(Long resourceId, Long permissionId) {
		resourceDao.removeResourcePermission(resourceId, permissionId);
	}

	public List<Resource> getAllResources() {
		return resourceDao.getAllResources();
	}

	public Resource read(Long id) {
		return resourceDao.read(id);
	}

	public Resource getResourceByName(String resourceName) {
		return resourceDao.getResourceByName(resourceName);
	}

}
