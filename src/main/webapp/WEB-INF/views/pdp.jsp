<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="rbac.pdp.domain.User"%>
<!DOCTYPE html>
<html>
<head>
	<title>PDP Editor</title>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
		    var currentTabIndex = "0";
	
		    $tab = $("#tabs").tabs({
		         activate : function (e, ui) {
		            currentTabIndex = ui.newTab.index().toString();
		            sessionStorage.setItem('tab-index', currentTabIndex);
		         }
		    });
	
		    if (sessionStorage.getItem('tab-index') != null) {
		        currentTabIndex = sessionStorage.getItem('tab-index');
		        console.log(currentTabIndex);
		        $tab.tabs('option', 'active', currentTabIndex);
		    }
		    $('#btn-sub').on('click', function () {
		        sessionStorage.setItem("tab-index", currentTabIndex);
		    });
		});
	
		function removeUser() {
			if (document.getElementById("userSelect").value == "")
				alert("Deve Selecionar o Utilizador a Remover");
			else {
				document.getElementById("userForm").action = "remUser.html";
				document.getElementById("userForm").submit();
			}
		}
	
		function removeRole() {
			if (document.getElementById("roleSelect").value == "")
				alert("Deve Selecionar o Role a Remover");
			else {
				document.getElementById("roleForm").action = "remRole.html";
				document.getElementById("roleForm").submit();
			}
		}
	
		function removePermission() {
			if (document.getElementById("permissionSelect").value == "")
				alert("Deve Selecionar a Permission a Remover");
			else {
				document.getElementById("permissionForm").action = "remPermission.html";
				document.getElementById("permissionForm").submit();
			}
		}
	
		function removeResource() {
			if (document.getElementById("resourceSelect").value == "")
				alert("Deve Selecionar o Resource a Remover");
			else {
				document.getElementById("resourceForm").action = "remResource.html";
				document.getElementById("resourceForm").submit();
			}
		}
		
		function removeUserRole() {
			if (document.getElementById("userRoleSelect").value == "")
				alert("Deve Selecionar a Associacao a Remover");
			else {
				document.getElementById("userRoleForm").action = "remUserRole.html";
				document.getElementById("userRoleForm").submit();
			}
		}
		
		function removePermissionRole() {
			if (document.getElementById("permissionRoleSelect").value == "")
				alert("Deve Selecionar a Associacao a Remover");
			else {
				document.getElementById("permissionRoleForm").action = "remPermissionRole.html";
				document.getElementById("permissionRoleForm").submit();
			}
		}
		
		function removeResourcePermission() {
			if (document.getElementById("resourcePermissionSelect").value == "")
				alert("Deve Selecionar a Associacao a Remover");
			else {
				document.getElementById("resourcePermissionForm").action = "remResourcePermission.html";
				document.getElementById("resourcePermissionForm").submit();
			}
		}
		
		function removeHRole() {
			if (document.getElementById("hRoleSelect").value == "")
				alert("Deve Selecionar a Associacao a Remover");
			else {
				document.getElementById("hRoleForm").action = "remHRole.html";
				document.getElementById("hRoleForm").submit();
			}
		}
		
		var options;

		function makeMeAGraph() {
		    
		    options = $('#options').serialize();

		    src = "https://chart.googleapis.com/chart?"+options
		    $('#graph').attr("src", src);
		}
	</script>
	<style>
	
	section {
	    border:1px solid gray;
	    padding:5px;
	    margin:15px;
	}
	
	label {
		width: 110px;
		display: inline-block;
	}
	
	input {
		width:170px;
	}
	
	select {
		width:170px;
	}
	</style>
</head>

<body onload="makeMeAGraph()">
	<h1>Hierarchical RBAC Designer (RBAC1) </h1>
	<div id="tabs">
		<ul>
			<li><a href="#tabs-1">Entidades</a></li>
			<li><a href="#tabs-2">Rela��es</a></li>
			<li><a href="#tabs-3">Hierarquia Roles</a></li>
			<li><a href="#tabs-4">Queries</a></li>
			<li><a href="#tabs-5">Sess�o</a></li>
			<li><a href="#tabs-6">Visualizar</a></li>
		</ul>
		<div id="tabs-1">
			<section>
				<form id="userForm" name="userForm" method="post" action="user.html">
					<h2>Users</h2>
					<label for="userName">User:</label>
					<input type="text" id="userName" name="userName" /> <input
						type="submit" value="Adicionar" /> <select id="userSelect"
						name="userSelect">
						<option value=""></option>
						<c:forEach var="user" items="${model.users}">
							<option value="${user.id}">${user.name}</option>
						</c:forEach>
					</select> <input type="button" value="Remover" onclick="removeUser()" />
				</form>
			</section>
		
			<section>
				<form id="roleForm" name="roleForm" method="post" action="role.html">
					<h2>Roles</h2>
					<label for="roleName">Role:</label>
					<input type="text" id="roleName" name="roleName" /> <input
						type="submit" value="Adicionar" /> <select id="roleSelect"
						name="roleSelect">
						<option value=""></option>
						<c:forEach var="role" items="${model.roles}">
							<option value="${role.id}">${role.name}</option>
						</c:forEach>
					</select> <input type="button" value="Remover" onclick="removeRole()" />
				</form>
			</section>
		
			<section>
				<form id="permissionForm" name="permissionForm" method="post"
					action="permission.html">
					<h2>Permissions</h2>
					<label for="permissionName">Permission:</label>
					<input type="text" id="permissionName"
						name="permissionName" /> <input type="submit"
						value="Adicionar" /> <select id="permissionSelect"
						name="permissionSelect">
						<option value=""></option>
						<c:forEach var="permission" items="${model.permissions}">
							<option value="${permission.id}">${permission.name}</option>
						</c:forEach>
					</select> <input type="button" value="Remover" onclick="removePermission()" />
				</form>
			</section>
		
			<section>
				<form id="resourceForm" name="resourceForm" method="post"
					action="resource.html">
					<h2>Resources</h2>
					<label for="resourceName">Resource:</label> 
					<input type="text" id="resourceName" name="resourceName" />
					<input type="submit" value="Adicionar" /> <select
						id="resourceSelect" name="resourceSelect">
						<option value=""></option>
						<c:forEach var="resource" items="${model.resources}">
							<option value="${resource.id}">${resource.name}</option>
						</c:forEach>
					</select> <input type="button" value="Remover" onclick="removeResource()" />
				</form>
			</section>
		</div>
		
		<div id="tabs-2">
			<section>
				<form id="userRoleForm" name="userRoleForm" method="post"
					action="userRole.html">
					<h2>User Roles</h2>
					<label for="userId">User:</label>
					<select id="userId" name="userId">
						<option value=""></option>
						<c:forEach var="user" items="${model.users}">
							<option value="${user.id}">${user.name}</option>
						</c:forEach>
					</select> 
					<label for="roleId">Role:</label>
					<select id="roleId" name="roleId">
						<option value=""></option>
						<c:forEach var="role" items="${model.roles}">
							<option value="${role.id}">${role.name}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Adicionar" />
					<select id="userRoleSelect" name="userRoleSelect">
						<option value=""></option>
						<c:forEach var="userRole" items="${model.userRoles}">
							<option value="${userRole.userId}_${userRole.roleId}">${userRole.userName} - ${userRole.roleName}</option>
						</c:forEach>
					</select>
					<input type="button" value="Remover" onclick="removeUserRole()" />
				</form>
			</section>
		
			<section>
				<form id="permissionRoleForm" name="permissionRoleForm" method="post"
					action="permissionRole.html">
					<h2>Permission Roles</h2>
					<label for="permissionId">Permission:</label> 
					<select id="permissionId" name="permissionId">
						<option value=""></option>
						<c:forEach var="permission" items="${model.permissions}">
							<option value="${permission.id}">${permission.name}</option>
						</c:forEach>
					</select> 
					<label for="roleId">Role:</label>
					<select id="roleId" name="roleId">
						<option value=""></option>
						<c:forEach var="role" items="${model.roles}">
							<option value="${role.id}">${role.name}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Adicionar" />
					<select id="permissionRoleSelect" name="permissionRoleSelect">
						<option value=""></option>
						<c:forEach var="permissionRole" items="${model.permissionRoles}">
							<option value="${permissionRole.permissionId}_${permissionRole.roleId}">${permissionRole.permissionName} - ${permissionRole.roleName}</option>
						</c:forEach>
					</select>
					<input type="button" value="Remover" onclick="removePermissionRole()" />
				</form>
			</section>
			
			<section>
				<form id="resourcePermissionForm" name="resourcePermissionForm" method="post"
					action="resourcePermission.html">
					<h2>Resource Permissions</h2>
					<label for="resourceId">Resource:</label> 
					<select id="resourceId" name="resourceId">
						<option value=""></option>
						<c:forEach var="resource" items="${model.resources}">
							<option value="${resource.id}">${resource.name}</option>
						</c:forEach>
					</select>
					<label for="permissionId">Permission:</label> 
					 <select id="permissionId" name="permissionId">
						<option value=""></option>
						<c:forEach var="permission" items="${model.permissions}">
							<option value="${permission.id}">${permission.name}</option>
						</c:forEach>
					</select> 
					<input type="submit" value="Adicionar" />
					<select id="resourcePermissionSelect" name="resourcePermissionSelect">
						<option value=""></option>
						<c:forEach var="resourcePermission" items="${model.resourcePermissions}">
							<option value="${resourcePermission.resourceId}_${resourcePermission.permissionId}">${resourcePermission.resourceName} - ${resourcePermission.permissionName}</option>
						</c:forEach>
					</select>
					<input type="button" value="Remover" onclick="removeResourcePermission()" />
				</form>
			</section>
		</div>
		<div id="tabs-3">
			<section>
				<form id="hRoleForm" name="hRoleForm" method="post"
					action="hRole.html">
					<h2>Hierarquia das Roles</h2>
					<label for="parentId">Pai:</label> 
					<select id="parentId" name="parentId">
						<option value=""></option>
						<c:forEach var="role" items="${model.roles}">
							<option value="${role.id}">${role.name}</option>
						</c:forEach>
					</select>
					
					<label for="childId">Filho:</label> 
					<select id="childId" name="childId">
						<option value=""></option>
						<c:forEach var="role" items="${model.roles}">
							<option value="${role.id}">${role.name}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Adicionar" />
					<select id="resourcePermissionSelect" name="resourcePermissionSelect">
						<option value=""></option>
						<c:forEach var="hrole" items="${model.hRoles}">
							<option value="${hrole.parentId}_${hrole.childId}">${hrole.parentName} - ${hrole.childName}</option>
						</c:forEach>
					</select>
					<input type="button" value="Remover" onclick="removeHRole()" />
				</form>
			</section>
		</div>
		
		<div id="tabs-4">
			<section>
				<h2>Roles By User</h2>
				<form id="queryUserRoleForm" name="queryUserRoleForm" method="post" action="queryUserRole.html">
					<select id="queryUserRoleSelect" name="queryUserRoleSelect">
						<option value=""></option>
						<c:forEach var="user" items="${model.users}">
							<c:choose>
								<c:when test="${user.id eq model.queryUserRoleId}">
									<option value="${user.id}" selected="selected">${user.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${user.id}">${user.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					
					<select size="5">		
						<c:forEach var="role" items="${model.rolesByUser}">
							<option value="${role.id}">${role.name}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Query" />
				</form>
			</section>	

			<section>
				<h2>Roles By Permission</h2>
				<form id="queryPermissionRoleForm" name="queryPermissionRoleForm" method="post" action="queryPermissionRole.html">
					<select id="queryPermissionRoleSelect" name="queryPermissionRoleSelect">
						<option value=""></option>
						<c:forEach var="permission" items="${model.permissions}">
							<c:choose>
								<c:when test="${permission.id eq model.queryPermissionRoleId}">
									<option value="${permission.id}" selected="selected">${permission.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${permission.id}">${permission.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					
					<select size="5">		
						<c:forEach var="role" items="${model.rolesByPermission}">
							<option value="${role.id}">${role.name}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Query" />
				</form>
			</section>			
			
			<section>
				<h2>Users By Permission</h2>
				<form id="queryPermissionUserForm" name="queryPermissionUserForm" method="post" action="queryPermissionUser.html">
					<select id="queryPermissionUserSelect" name="queryPermissionUserSelect">
						<option value=""></option>
						<c:forEach var="permission" items="${model.permissions}">
							<c:choose>
								<c:when test="${permission.id eq model.queryPermissionUserId}">
									<option value="${permission.id}" selected="selected">${permission.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${permission.id}">${permission.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					
					<select size="5">		
						<c:forEach var="user" items="${model.usersByPermission}">
							<option value="${user.id}">${user.name}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Query" />
				</form>
			</section>			
			
			<section>
				<h2>Permissions By Resource</h2>
				<form id="queryPermissionResourceForm" name="queryPermissionResourceForm" method="post" action="queryPermissionResource.html">
					<select id="queryPermissionResourceSelect" name="queryPermissionResourceSelect">
						<option value=""></option>
						<c:forEach var="resource" items="${model.resources}">
							<c:choose>
								<c:when test="${resource.id eq model.queryPermissionResourceId}">
									<option value="${resource.id}" selected="selected">${resource.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${resource.id}">${resource.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					
					<select size="5">		
						<c:forEach var="permission" items="${model.permissionsByResource}">
							<option value="${permission.id}">${permission.name}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Query" />
				</form>
			</section>			
			
		</div>
		<div id="tabs-5">
			<section>
				<h2>Session Queries</h2>
					<form method="post" action="session.html">
					<select id="sessionSelect" name="sessionSelect">
						<option value=""></option>
						<c:forEach var="session" items="${model.userRoles}">
							<c:set var="tmpSelId" value="${session.userId}_${session.roleId}" />
							<c:choose>
								<c:when test="${tmpSelId eq sessionId}">
									<option value="${tmpSelId}" selected="selected">${session.userName} - ${session.roleName}</option>
								</c:when>
								<c:otherwise>
									<option value="${tmpSelId}">${session.userName} - ${session.roleName}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					<input type="submit" value="Query" />
					<select size="15" style="width:300px">
						<c:forEach var="sessionResult" items="${model.sessionResult}">
							<option value="${sessionResult.userId} - ${sessionResult.parentRoleId} - ${sessionResult.childRoleId}  - ${sessionResult.permissionId} - ${sessionResult.resourceId}">
								${sessionResult.userName} - ${sessionResult.parentRoleName} - ${sessionResult.childRoleName} - ${sessionResult.permissionName} - ${sessionResult.resourceName}
							</option>
						</c:forEach>
					</select>
				</form>
			</section>
				
			<section>
				<h2>Has Permission</h2>
					<form method="post" action="hasPermission.html">
					<label for="hasPermUserId">User:</label>
					<select id="hasPermUserId" name="hasPermUserId">
						<option value=""></option>
						<c:forEach var="user" items="${model.users}">
							<option value="${user.id}">${user.name}</option>
						</c:forEach>
					</select>
					
					<label for="hasPermRoleId">Role:</label>
					 <select id="hasPermRoleId" name="hasPermRoleId">
						<option value=""></option>
						<c:forEach var="role" items="${model.roles}">
							<option value="${role.id}">${role.name}</option>
						</c:forEach>
					</select>
					
					<label for="hasPermResourceId">Permission:</label>
					 <select id="hasPermResourceId" name="hasPermResourceId">
						<option value=""></option>
						<c:forEach var="resource" items="${model.resources}">
							<option value="${resource.id}">${resource.name}</option>
						</c:forEach>
					</select>

					<input type="submit" value="Query" />
					
					<input type="text" value="RESULT: ${model.hasPerm}" />
					
				</form>
				
			</section>
			
			<section>
				<h2>Has Permission Name</h2>
					<form method="post" action="hasPermissionName.html">
					<label for="hasPermUser">User:</label>
					<select id="hasPermUser" name="hasPermUser">
						<option value=""></option>
						<c:forEach var="user" items="${model.users}">
							<option value="${user.name}">${user.name}</option>
						</c:forEach>
					</select>
					
					<label for="hasPermRole">Role:</label>
					 <select id="hasPermRole" name="hasPermRole">
						<option value=""></option>
						<c:forEach var="role" items="${model.roles}">
							<option value="${role.name}">${role.name}</option>
						</c:forEach>
					</select>
					
					<label for="hasPermResource">Resource:</label>
					 <select id="hasPermResource" name="hasPermResource">
						<option value=""></option>
						<c:forEach var="resource" items="${model.resources}">
							<option value="${resource.name}">${resource.name}</option>
						</c:forEach>
					</select>
					<input type="submit" value="Query" />
					<input type="text" value="RESULT: ${model.hasPermName}" />	
				</form>
			</section>
			<section>
				<h2>Has Permission Full Name</h2>
					<form method="post" action="permissionName.html">
					<label for="permUser">User:</label>
					<select id="permUser" name="permUser">
						<option value=""></option>
						<c:forEach var="user" items="${model.users}">
							<option value="${user.name}">${user.name}</option>
						</c:forEach>
					</select>
					
					<label for="permRole">Role:</label>
					 <select id="permRole" name="permRole">
						<option value=""></option>
						<c:forEach var="role" items="${model.roles}">
							<option value="${role.name}">${role.name}</option>
						</c:forEach>
					</select>
					
					<label for="permPermission">Permission:</label>
					 <select id="permPermission" name="permPermission">
						<option value=""></option>
						<c:forEach var="permission" items="${model.permissions}">
							<option value="${permission.name}">${permission.name}</option>
						</c:forEach>
					</select>
					
					<label for="permResource">Resource:</label>
					 <select id="permResource" name="permResource">
						<option value=""></option>
						<c:forEach var="resource" items="${model.resources}">
							<option value="${resource.name}">${resource.name}</option>
						</c:forEach>
					</select>
					<input type="submit" value="Query" />
					<input type="text" value="RESULT: ${model.permName}" />
				</form>
			</section>
			
		</div>
		<div id="tabs-6">
			<section>
				<div id="options-div" style="display: none;"> 
				    <form id="options">
				        <fieldset>
				            <legend> DOT code for graph </legend>
				            <textarea name=chl id=chl > 
					      		${model.render}      
				        	</textarea>
				        </fieldset>
				        <fieldset>
				            <legend> Graphviz Engine </legend>
				            <label> <input type=radio name=cht value="gv" checked=checked> dot </label>
				            <label> <input type=radio name=cht value="gv:neato"> neato </label>
				            <label> <input type=radio name=cht value="gv:twopi"> twopi </label>
				            <label> <input type=radio name=cht value="gv:circo"> circo </label>
				            <label> <input type=radio name=cht value="gv:fdp"> fdp </label>
				        </fieldset>
				        <input type="button" onclick="makeMeAGraph()" value="Make me a graph!" />
				    </form>
				</div>
				<div id="graph-div">
					<img id="graph" />
				</div>
			</section>
		</div>
	</div>

</body>
</html>